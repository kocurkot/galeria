from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'appinz.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'galeria.view.home.index'),
    url(r'^index$', 'galeria.view.home.index'),
    url(r'^info$', 'galeria.view.home.info'),
    url(r'^gallery/list$', 'galeria.view.gallery.list'),
    url(r'^gallery/make$', 'galeria.view.gallery.make'),
    url(r'^gallery/addPhotos$', 'galeria.view.gallery.addPhotos'),
]
