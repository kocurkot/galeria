from django.http import HttpResponseRedirect
from django.shortcuts import render
from galeria.forms import MakeAGalleryForm
from galeria.forms import MakeAddPhotosForm
from galeria.models import Galeria
import os

def list(request):
    return render(request, 'gallery/list.html', {'galleryList': getGalleryList()})

def make(request):
    if request.method == 'POST':
        form = MakeAGalleryForm(request.POST)
        if form.is_valid():
            tytul = request.POST.get('tytul', '')
            styl = request.POST.get('styl', '')
            saveOb = Galeria(tytul = tytul, styl = styl)
            saveOb.save()
            request.session['galleryName'] = tytul
            return HttpResponseRedirect('/gallery/addPhotos')
    else:
        form = MakeAGalleryForm()
        return render(request, 'gallery/make.html', {'form': form})

def make_folder(galleryName):
    if not os.path.exists("uploadedFiles\\"+galleryName): os.makedirs("uploadedFiles\\"+galleryName)

def handle_uploaded_file(f, dir):
    destination = open('uploadedFiles\\'+dir+'\\'+f._name, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()

def addPhotos(request):
    if request.method == 'POST':
        tytul = request.session['galleryName']
        make_folder(tytul)
        for afile in request.FILES.getlist('myfiles'):
            handle_uploaded_file(afile, tytul)
        return HttpResponseRedirect('/gallery/list')
    else:
        return  render(request,'gallery/addPhotos.html')

def getGalleryList():
    galleryList = []
    for name in os.listdir("uploadedFiles"):
        galleryList.append(name)
    return galleryList

