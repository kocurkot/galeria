from django.shortcuts import render

__author__ = 'kocur_000'

# 21 Temat: Generator galerii zdjęć HTML
# Wymagania do projektu: aplikacja powinna pozwalać na zbudowanie listy
# zdjęć do galerii, wyboru wyglądu galerii spośród kilku predefiniowanych
# szablonów. Wygenerowana strona powinna wyświetlać miniatury
# wygenerowane przez aplikację a po kliknięciu zdjęcie w pełnej rozdzielczości.
# Technika wykonania aplikacji: Python + GUI, HTML5

def index(request):
    return render(request, 'home/index.html')

def info(request):
    return render(request, 'home/info.html')