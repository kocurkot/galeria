from django import forms

class MakeAGalleryForm(forms.Form):
    tytul = forms.CharField(label='Tytuł', max_length=100)
    styl = forms.ChoiceField(choices=[(x, x) for x in range(1, 5)])

class MakeAddPhotosForm(forms.Form):
    sciezka = forms.FileField(label="plik")
    tytul = forms.CharField(label='Tytuł', max_length=100)
